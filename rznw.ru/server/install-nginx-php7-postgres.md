## Установка на шаблонную ОС Debian 8

Имеем ip адресс и пароль для root. Вхожу в консоль через putty и понеслась.

Сразу же обновляю пакеты apt:
```
apt-get update
apt-get dist-upgrade
```
Установка часового пояса: `dpkg-reconfigure tzdata`
запускаем конфигуратор и выбираем нужные параметры (Europe -> Moscow)

Локаль:  `dpkg-reconfigure locales` (выбрать ru_RU.UTF-8 849)

Установил mc:
```
apt-get install mc
```

Системный эдитор пусть mc. Обычно перед первым редактированием любого файла система спросит какой редактор использовать - вибираем mcedit.
```
export EDITOR='mcedit' 
```

Установить curl
```
apt-get install curl
```


## Установка последней nginx (для тестовой подержки http2)

Есть желание установить самую последнюю версию для экспериментов с http2. Для этого добавляем в /etc/apt.sources.list строки:

Строки узнать надо здесь: [nginx.org](http://nginx.org/ru/linux_packages.html)

Выбрать надо из 3-х вариантов: стабильная ветка: 
```
deb http://nginx.org/packages/debian/ jessie nginx
deb-src http://nginx.org/packages/debian/ jessie nginx
```
Последняя ветка: 
```
deb http://nginx.org/packages/mainline/debian/ jessie nginx
deb-src http://nginx.org/packages/mainline/debian/ jessie nginx
```

Выбираю последнюю и добавляю.

### Решение проблемы с отсутствием ключа
Однако перед запуском `apt-get update` публичный ключ. Если этого не сделать вместо хорошего показывается это:  
*GPG error: http://nginx.org jessie Release: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY ABF5BD827BD9BF62*

```
apt-get install debian-keyring debian-archive-keyring
apt-key update

gpg --keyserver subkeys.pgp.net --recv ABF5BD827BD9BF62

gpg --export --armor ABF5BD827BD9BF62 | apt-key add -
```

Установил nginx:
`apt-get install nginx nginx-extras`


## Установка PHP7 и начально необходимых модулей

Статья про устанвоку [пакетов php7](//www.colinodell.com/blog/2015-12/installing-php-7-0-0)

Добавить в `/etc/apt.sources.list`
``
deb http://packages.dotdeb.org jessie all
deb-src http://packages.dotdeb.org jessie all
``

Скачиваем файл с публичным ключем
```
wget --no-check-certificate https://www.dotdeb.org/dotdeb.gpg
```
Добавляем ключ. После добавления ключа файл можно удалить.
```
apt-key add dotdeb.gpg
apt-get update
```
То, к чему все шло:
```
apt-get install php7.0 
//или
apt-get install php7.0-fpm // поставил это для работы со связкой nginx
```
### Модули php 7

Превым источником попался этото: (Install PHP 7 Modules)[//tecadmin.net/install-php-7-on-ubuntu/]

Узнать какие пакеты с модулями доступны: `apt-cache search php7-*`

php7.0-opcache, php7.0-json - уже оказались установлены

Из появившегося списка нужно выбрать необходимые и составить команду:
```
apt-get install php7.0-pgsql php7.0-curl php7.0-json php7.0-redis php7.0-xdebug php7.0-gd php7.0-intl
```
Должно пройти без проблем.

## Postgres
(Про устанвоку postgres на wheezy)[http://www.pontikis.net/blog/postgresql-9-debian-7-wheezy] - для Debian 8 (jessie) - то же самое но подставлять другое кодовое имя.
http://www.postgresql.org/download/linux/debian/

Создаем файл  /etc/apt/sources.list.d/pgdg.list, и добавляе м внего строку:
`deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main`

```
wget  --no-check-certificate https://www.postgresql.org/media/keys/ACCC4CF8.asc - скачивает файл ключей и добавляет его - gоnом его можно удалить
apt-key add ACCC4CF8.asc
```

И в финале получаем последнюю версию Postgres:
```
apt-get install postgresql
```


## Установка хоста

Про настройку nginx-PHP-fpm:
http://pektop.net/2013/09/sovety-po-nastrojke-i-optimizacii-nginx-i-php-fpm/


## Установка git

## Установка Начало проекта phpStorm - деплой и настройка отдаленного дебага

