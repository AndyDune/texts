#!/bin/bash

#chmod +x /home/mdirs.sh
# Example
#./mdirs.sh site telegram-posting.ru
user=$1
domain=$2

home_dir='/home/'$user

if  ! [ -d $home_dir ]; then
	echo "No home dir: $home_dir"
	exit 0
fi

site_main_dir=$home_dir'/'$2

if  [ -d $site_main_dir ]; then
	echo "Have dir: $site_main_dir"
	exit 0
fi


dir=$site_main_dir
mkdir $dir
chown $user $dir
chgrp $user $dir

dir=$site_main_dir'/log'
mkdir $dir
chown $user $dir
chgrp $user $dir

dir=$site_main_dir'/tmp'
mkdir $dir
chown $user $dir
chgrp $user $dir

dir=$site_main_dir'/project'
mkdir $dir
chown $user $dir
chgrp $user $dir

