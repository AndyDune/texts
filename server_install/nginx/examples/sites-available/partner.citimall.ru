server {
    server_name partner.citimall.ru;
    listen [::]:80;
    listen 80;

    return 301 $scheme://citimall.ru$request_uri;
}
