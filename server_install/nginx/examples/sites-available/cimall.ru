server {
  server_name www.cimall.ru;

  listen [::]:80;
  listen 80;


  # and redirect to the non-www host (declared below)
  return 301 $scheme://cimall.ru$request_uri;
}

server {
    server_name cimall.ru *.cimall.ru;
    listen [::]:80;
    listen 80;

    set $root_path "/var/www/cimall.ru/data/public"; # корневая директория
    set $root_site_tmp "/var/www/cimall.ru/tmp"; # корневая директория


    root $root_path;
 
    access_log  /var/www/cimall.ru/tmp/nginx/access.log;  #расположение логов данного хоста
    error_log  /var/www/cimall.ru/tmp/nginx/error.log;


 
    location / {
        index index.php index.html index.htm;
	if (!-e $request_filename){
	    rewrite ^(.*)$ /local/urlrewrite.php last;
        }
    }
 
     location ~* \.(css|js|gif|png|jpg|jpeg|ico|eot|ttf|woff)$ {
        root         $root_path;
        access_log   off;
        expires      3d;
	error_page 404 /404.html;
     }


    location ~* \.(eot|ttf|woff)$ {
	add_header Access-Control-Allow-Origin *;
    }

    location ^~ /bitrix/modules {
	deny all;
    }

    location ~ \.php$ {
        fastcgi_pass   php5-fpm-sock;
        fastcgi_index  index.php;
	fastcgi_intercept_errors on;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;

        include        fastcgi_params;

	location ~ ^/bitrix/modules/(.*)\.php$ {
		deny all;
	    }
	location ~ ^/local/modules/(.*)\.php$ {
		deny all;
	    }


	fastcgi_param PHP_ADMIN_VALUE 'session.save_path="1;$root_site_tmp/session_files"\nmbstring.func_overload=2\nmbstring.internal_encoding="UTF-8"\nrealpath_cache_size="4096k"\nshort_open_tag="on"\nupload_max_filesize="8M"\nopen_basedir="$root_path/:$root_site_tmp/session_files/:$root_site_tmp/upload_tmp_dir/"\nupload_tmp_dir="$root_site_tmp/upload_tmp_dir"';



        if (!-f $request_filename) {
            rewrite  ^(.*)/index.php$  $1/ redirect;
	}
	set $test_file "bitrix/html_pages/$host$1@$args.html";
	if ( -f "$document_root/$test_file" ) { set $usecache "${usecache}F"; }
	if ($usecache = "ABCDEF" ){ rewrite .* /$test_file last; }

    }
    include bitrix/bitrix_general.conf;
}
#include common/upstream;
