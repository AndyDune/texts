<?php

namespace Sprint\Migration;

use Sprint\Migration\Helpers\EventHelper;

class Version20160613130707 extends Version
{

    protected $description = "Создание события для отправки пароля пользователю ежели он из 1С";

    public function up()
    {
        $description =
            '#EMAIL# - email пользователя
            #NAME# - полное имя пользователя
            #DOMAIN# - момен сайта
            #PASSWORD# - роль менеджера seller или manager
            '
        ;

        $helper = new EventHelper();

        $helper->addEventTypeIfNotExists('SEND_PASSWORD_1C', [
            'NAME'        => 'Отсыл пароля пользователю',
            'DESCRIPTION' => $description,
            'LID' => 's1',
            'SORT' => 150
        ]);


        $helper->addEventMessageIfNotExists('SEND_PASSWORD_1C', [
            'ACTIVE' => 'Y',
            'EMAIL_FROM' => '#SHOP_EMAIL#',
            'EMAIL_TO' => '#EMAIL#',
            'SUBJECT' => 'Ваш пароль для входа в интернет-магазина детская обувь',
            'BODY_TYPE' => 'text',
            'SITE_ID' => 's1',
            'LID' => 's1',
            'MESSAGE' => '
Ваш логин: #EMAIL#
Ваш пароль #PASSWORD#
Сайт: #DOMAIN#
            ',
        ]);


    }

    public function down()
    {

    }

}
