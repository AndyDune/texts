# Node.js ����������: ������ ������� ��� ���������� ����������

������������ ������: [Node.js Frameworks: The 10 Best for Web and Apps Development](http://noeticforce.com/best-nodejs-frameworks-for-web-and-app-development)

## ����������

������ JavaScript ��� �������� �������� ���������� ��������� ��� ��������� JavaScript �, ����������, ����� �� ����� ��������� � ���� ���������� ���-����������.

��� ���, ��� �� � ����, Node.JS ��� �����, ������� ��������� ��������� ��� JavaScript �� �������, ��� ��������. �� �������� �� ������ ������������ ������ Chrome V8 �� Google, ������� �������������� JavaScript � �������� Chrome.

����� ����� ������������ Nodejs ����� ���������� ���, ��� ������������ �������� ����������� ��������� ������� ��� ��� ���������� � �������� ��� � �� ������� ��������� ������ ���� ���� JavaScript.

����� �� node ������ ��� ����������, �������� ����������, ������� ����� ��� ��������� �������� ����������. ���������� ��������� Node.js �����������, ������� ��������� ��������� ��������� ��������� ���������� ��� ������������� ������������ ������ �������� �� �������, ����� ��� ���-������, ������ ����������.

�������������� ����������, ����� ��� Express, Koa � Hapi ����� � ��������� ��������� ��������� � ���������� ������ ��� ��� ��� �����. �� � ���� ������ ���������� ����� ��������� ����� ������� � ������ �� ���������� � ���������� ��������, ������� ��������� ���� ������������ (��� �� ����).

� ������ �������,  ����������� ����������, ���� ���  Mean.io, Meteor, Derby � Mojito ������������ �� ����������� ���������� � �����������. ��� ����� ������. ��� �� ���� ����� ������� ������� � ������ ������� ����������.

������� ��������� �� ������ � �������� ������ NodeJs ����������, ��������� �� ������� ������. ����������, ������� �������� ��� ������� �������������� ���������� ����� ��������, ���������� � �������� �������.

## 1. Node.js Express

Express ��� "node.js Express" �� ����� ���� �� ��������� � ������������� �������������, �������� � Node.js, �� ��� ������ ���� �� �� ������ ��� ���������� ����� � express ������ ��� �� �������� � ������ ������ node.js �����������. Express �������� �������������� � ������ ����������� ������������ ��� ����������� NodeJs � ������ ��� ��������� ������� ���������� ��� ��� API.

�� ���������� ��� �������, ����������� ��� ���������� �������� ��� � ��������� ����������, � ����� ��� �������� ����������� API. ������������ ����� ������ ������� ��� ������ ���������� ��� �������� � ����������� ��� ���������� ����. Express ������ �� ����� ��������������� � ���� ������� �������� � ��������� �������:

- ������������ ����� ��������� ���� ������ � ��������� ���� ������.
- ����������� ����� ������ �������������� �������������
- ������������ ����� ������������� � ������ �����������
- ���������� ����� ��������� ����� ��� ���������� 

� Node Express �� ������ ������� �� �� ����� ��� � � ������ node �� � ������� ������� ������ ����� ����. �������, �� ������ ���������� middleware (������������� ����������� �����������) ��� ��������� http ��������, ����������� ��������� �� htth ������� ��� ����� � ��� ������������� �������� ��� ������������ �������.

������������ �� ������ ������ � ����������� ����� ������ �������. Nodejs Express ������ �������� ��� �������� � �������� node. Express ����� ������������ �������� � ���������� �����������, �������� ������������� � ����� ������� ����� ������������� ����� ���� ����������� node.js.

Express �������� � ���� ��������� ��������� ���� node.js. ����� �������� MIT � � ��������� ����� ������������ StrongLoop.

�� ������ ������ ������ � Express node.js �� ����������� ����� � [expressjs.com](http://expressjs.com/).

## 2. Sail.js (node.js mvc)

Sail ������ ���������. �� ������������ ������� �� �����  ���������, ������� ��������� ��� ������������ ���������� ������ ��������.

Sail.js ��������� ���������������� �� ���������� Express, ������� ����� � ��� ������, ��� ��������� �������� http � sockets.IO ��� ��������� � ��������. ������ ����������� �� ���������, Sail ���� �������� � ������ ���������� ����� ������ ���������.

� ������ ���������� ������ waterline, ����������� �� ���� ���� ������ ORM, ������� ��������� ��������� �������� � ����� ��. �� ������ ���� ������� ���� � ������������ ���� � ����� �����. � ������� ����� � ����, ��� �� ������ ���������� �� ����������� ��� ������ � NoSQL ����, ����� ��� MongoDB ��� ����� �� ������ ������.

Sail �������� ����� � ����� �������� ��� ��������������������� ���������� ����, �������������� ��� ����������, �������������� ������ � ������ ����������, ��� ����� �������� ������ ������� � �������� �������. ���� �� �������� � Ruby, Django ��� Zend ��� ����� ��������� ������������ Sail.js.

� ���� ������, �� ��������� ����� ������������ ����������� MVC (sail - nodejs MVC ���������), ������ �������� � ������ ��������� � ���� ���� ������ ��� ������ �������.  Sail.js ������ ���� ������� � �������������� Treeline and balderdash. �� ��������� � ����� �������� MIT.

��������� � Sale ����� �������� �� ����������� ����� � [sailsjs.org](http://sailsjs.org/).

## 3. KOA

KOA ��� ������ ������� � ���� node.js ����������� (MVC ����������) � ������������ ����� �������� ����������. ������ �������� ������ ��� ������ ������ � ��������� �������� 2016 ����. �������� ����� ������������� �������� ����� ��� Express. ��������� ���� TJ Holowaychuk. ��� �� ����� KOA ����� �������� �� Express ������ � ��������� ��������.

���������� ES6 - ��� ���� ����� ���������� KOA. ���������� ������������ ��� �������� ���������� ������������� �������� (middleware) ��������� ��������� ������ try/catch � ��������� ��� ��������� � �������� ��������� ������. 

KOA ����� ��������� ������ � ���� ��������� ���������. ������ �� ��� ������� ����� ��������� ������� � ������ �����������������. �������� ������� �������������� ���������� � ������� �������� ����� ����������.

������ ���� ������  JavaScript/Node.js ������������ �������� KOA ������ Express ��� ����� ��������, ��������� ��� ���� �� ������� ��������, ����� �������� ����������� ���, ��-������.

��������� � KOA ����� �������� �� ����������� ����� � [koajs.com](http://koajs.com/).


## 4. Meteor

��������� Meteor - ��� ������� ���� ���� full-stack ����������� ���������� �� Node.js. � ����� ��� 25 ���. ���������� GitHub, ������� ���������������� �������, �������� ���������� �����������, ������� ���������� ������������� Meteor - ��� ����������� ������. ��������� Meteor  �� ������� ��� � ��������� ���������� �� ������ JavaScript.

����� ���������� � Meteor - ��� ��, ��� ��� �������� ��� ����, ����� ����� ���������. �� ���������� � ����� ������ � ��������� ������ �� ���������� �� ������� �������. ������������ ������ API ��� ��� �������� �� �������, ��� � ��� ������� (��������, ��������� ��������).

�������� DDP ��������� ��� ������������ � ��� ������ � �������, ������� � ������� ��� ������ �� ������������ ��������. Meteor ����� ���� ����������� ������������ �� ����������� ����������, �� �������� ���������� ������ ��� ������ ����������� ����������. ���� �� �� ��������� ������������� � ������� ������������, �� ����� ������ �������� ������� �� ������ ����������, ����� ������ Meteor, ����������� ��� ������������ � �� ������ ��������� ��������� ��� �������� �������� ����������.

Meteor ������������ ���������������� ������� ������������� � ������� ���������������, ��� � ���������� Meteor ��� ����� �� �������. 

�������� ��� Meteor ������ �������� �� ����� - [meteor.com](http://meteor.com).

## 5. Derby.js

Derby.JS - ��� full-stack ���������, ������� �������� ����������� � Meteor, Mean.io � Mojito. �� �������� ������ node.js � ������ � MongoDB � redis. Derby ������������� ������� Racer ��� ������������� ������, ������� ��������� ����� �������������� ������ ����� ����� ������, �������� � ���������.

Racer ������ ����������, ���������� �� ���������� Derby, ����� ��������, ��� � ��������, ��� � �� �������. ��� ��������� ����� ��� ����-���������� ����������. Derby ����� ���������� �� ������� ������ Meteor, ������� ������ ������ ���� � ������ ���������� ������� ������ ������������ "�� �������". � Meteor ����� ����� ��������� ������� ���-���������� �� ������� ������� �����.

������ Derby ����� �������� ��� �������� ����� �������, ������ � ����������� ���������� �� ���� ���������� �������. �������� Derby ����������� � ��������� �����, �� �� �� �������� � ����� ����������, ����� ������ ��������� ����� � ������������ full-stack �����������.

�������� ��� Derby �� ����������� ����� - [derbyjs.com](http://derbyjs.com).

## 6. Flatiron.js (Node.js MVC Framework)

��������� ���������� Flatiron ���������� ��� ����� ������� ���� ����������� full-stack ���������. ��� ���� ����� ������������ ��� ������, ������� ���� � Flatiron, ��� � ������ �� ��������� ����������. ��, ��� ����, � ��� ��� ��������. ������ ��� ������ ����� ������� � ���� ������������ ��� �����, ����� �������� ������ ����� ��������� ������ ������ �����������.

���� ��������� ����� ������� �������������� full-stack ������� ��������� �����������, ��������� ��� ������������� ���������� ���� �� �����. �� Flatiron ������ ��������� Director, ������� ����� ��� ������������� �������� � ������� ���������� ������ ������ JavaScript ��� �����-���� ������������.

��� ������������ ������������ ������ Plates, ����� ��� ���������� ������� �������������� ����� ������������ ���� � ���������� ������ json. ��� ��������� (API) ����� ���� ����������� ��� ����� ��� ������. Flatiron �������������� Nodejitsu � ������� ������� ����������. ������ �����������, � ����� �������� �� ������ �� ������� ������������.

������ ��� Flatiron �� ����� [flatironjs.org](http://flatironjs.org).



## 7. Hapi

Hapi ���� �� �������� Node.js ����������, ������� �� ������� �� Express. ��� ������������ ����� ���������� �������� ��������� ���������� �� Express. ��� ������ Hapi ������������ ���������� ��� Express ��� ������ �����������, ������� ��������� ��� ������ Express.

Hapi �� ��� ����� ������� � ����� ������ ����������� Node.js, �� �� ����� ��� ���� �������� ���������� � ���� ��� ����������. Hapi ���������� ��������� ������������� HTTP ������, �������� ������������� � ������� ������. ��������� ������������ ��������� ������ ����� ������������ ��� ��� ��� ��������.

���������� Hapi ��� ������ ������ �������� � ��������, ������� ����� �������� �� Walmart. ��������� ������ ���� ����������, ������ ���������������� ��� ��������� MIT, ��� ��������� � �������� �����, �������� ��������� ��� ���������� � �������������.

������������ ����� ���������� ����� ������� �� ����� ��������� ��� Disney, Yahoo, Pebble, beats music � Walmart (������� ��), ������� ���������� Hapi ��� ��������� ����� �������.

������ ��� Hapi �� ����� - [hapijs.com](http://hapijs.com).


## 8. Mean.IO

MongoDB, Express, Angular � Node.js - ��� ���������� ������ � ������ Mean, ��� ��-�������� ��������, ��� �� ��������� ���� ������ � ����� ������, ��������� ����� � �����-���. ������ ����� ������ ������ ����������� ��� ����������.

Mean - ��� ����-���� ���������, � ������ ������� ������ ��� �����������, ����� ������ ������ ���������� � ������� ������������, ������� ������������� ������ �������� ���� � ������, ��� �������� ���������� ������ ������� � ���������.

�������� ���� ���������, ������������ ����������� �� ������������� ��������� � ��������� ��������� ����������. � Mean �� �������������� �� ��������� ������� ����������� �� �����������. ��� ���� ����������� Mean.IO - ��� ���� ����������� ������������ ������ JavaScript, �� ������� (������ json) � MongoDB �� ��������� ����� � Express � Node � ���������� � Angular.

������ ��� Mean.IO �� ����� - [mean.io](http://mean.io).


## 9. Mojito

Mojito ��� ������� ��������� Yahoo � ���������� ���� ����������. ��� �� ����� ��� ��������� � ������ ��� ������ ����� ����������� ������������ ��� Meteor ��� Mean.

Mojito ��� �� ���������� MVC �������� ��� ����������, ������������ ��� �������� ������� ��� � ��������� ���������� � �������������� HTML5, JavaScript � CSS3. ������� ����� Mojito - ���� ��������� ��� ������������� ������������ �����-�������������� ����������, ������� ����� �������� ��� �� ������� ��� � � ������� � ������� ���������.

��������� ��� Mojito ������ �������� �� ����� Yahoo ������������� - [mojito](https://developer.yahoo.com/cocktails/mojito/).

������, ��� ��������� ������ ��� Mojito ��� ������ ����� ���� �����.

## 10. Socket Stream

SocketStream - ���������� ���������, ������� �������� �� ������� ����� ������� � �������� ������� ����� �������� � ��������. ��� ��������, ��� ����������� ������ ����� ��������� ���������� ��������� � ��� ���������� �� ������� � ��� �������� �� ������� �� �������� �� ����������, ������� ���������� ������ ��� ������.

����� �����������, ��� ��������� �� ������������ ��� � ������������� ������������ ���������� ����������, ��� ������ ��� ORM. � �� ����� ������� ��� �� ������� ������ Sail.js, ������� � �������� ������ ���� �����. SocketStream ����� ����� � �������� �������������� ����������, ��������������������� ���, �����, ������� ����������, �������� �������� � ����� ����������, ��� ���� ����� �������� ����� ������� ����� �������� � ��������.

JSON ������������ ��� �������� ������ �� ������� � ������� � ��������. ����� ���������� ������ ��������� ������������ �� ������ ����� ��������. ������ ������ ����� ������ ������� ������ �������� � � ��������� ������ �������������� �������� ���� ��������. ������ ���� ����� � ������� � ����������� ����� ����� ����������.

������ ��� SocketStream �� ����� - [socketstream.com](https://socketstream.com/).

������� � ������ ����������: total.js, Geddy.JS, Locomotive, compound, Restify � strapi.io.

## � ����������

�������� � ���������� ��� ���������� �������� ����� ������, ��� ��� ������������ � ����������� ������� ���������� ���������� ��� �������� �������������� ��������. ��������� ������������ �� ������������� node.js �����������, ����������� � ���, ��� ��� ���������� ��������������� ��������� ����� "�� �������", � �� ������ ����� ��������������� �� ��������������� ���������� ������ ����, ����� ������� ������ �� ���������� � �������� ������� �������.

���������� ���� ������� ������ �����������, �������� �� ������ ��������� ��������������� ���������, �� �������� ������ ����� �������� ���������� �����������������, ����������� � ����������� ����������. ����������� ����� �������� ���������. ���������� ���� ���������� �������� ������� ��� Node.js �� ����������� ����.